#!/bin/bash

# Run an expensive command as frequently as you want through this.
# Command output will be kept for $cacheperiod (default 60s) and replayed for subsequent calls
# If running the commands is mutial exclusive and will be waited on for $timeout seconds (default 30s)
# Usage: ./runcached.sh expensivecommand params
# with custom values:
# cacheperiod=10 timeout=5 ./runcached.sh expensivecommand params

cmd="$*"
cmdmd5=$(echo -n "$@" | md5sum | cut -d\  -f1)
cachedir="/tmp"
cachedirmd5="${cachedir}/${cmdmd5}"
cmdlockfile="${cachedirmd5}-runcached.pid"
cmddatafile="${cachedirmd5}.data"
cmddatafile_tmp="${cmddatafile}.tmp"
cmdexitcode="${cachedirmd5}.exitcode"
cmdexitcode_tmp="${cmdexitcode}.tmp"

# Random sleep to avoid racing condition of creating the pid on the same time
sleep .$(( $RANDOM % 10 + 1 ))s

output_data() {
	cat "$cmddatafile"
	exit "$(cat "$cmdexitcode")"
}

run_command() {
	timeout=${timeout-30}
	while [[ -f "/tmp/${cmdmd5}-runcached.pid" ]]
	do
		if [[ "$count" -le 0 ]]
		then
			echo "Error: Timeout while waiting for command to terminate." >&2
			exit -1
		fi
		((count--))
	done

	echo $$ > "$cmdlockfile"
	$cmd &> "$cmddatafile_tmp"
	echo "$?" > "$cmdexitcode_tmp"
	rm "$cmdlockfile"
	mv "$cmddatafile_tmp" "$cmddatafile"
	mv "$cmdexitcode_tmp" "$cmdexitcode"
}

if [[ -r "$cmddatafile" ]]
then
	lastrun=$(stat -c %Y "$cmddatafile")
	currtime=$(date +%s)
	diffsec=$(( (currtime - lastrun) ))
	[[ "$diffsec" -ge "${cacheperiod-60}" ]] && run_command
else
	run_command
fi

output_data
